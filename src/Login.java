
public class Login implements Readable{
	
	private String email;
	private String password;
	
	public Login(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String LoginData(String email) {
		return email + " has been logged in.";
	}
	
	
}
